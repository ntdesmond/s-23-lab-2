# Lab 2 -- Linter and SonarQube as a part of quality gates

## Homework task

As a homework you will need to add automatical SonarCloud check to your CI. SonarCloud is a simplified version of SonarQube. [you can find it here](https://sonarcloud.io). Once again, lab should consist all of the checks, for this lab you may mark your SonarCloud check with `allow_failure: true`, Your resulting pipeline should have three steps: `build` -> `linter` -> `sonar_cloud`. As well you should attach screenshots of SonarQube check to your readme file.
**Lab is counted as done, if your pipelines are passing.**

## SonarQube local setup

![sq_screenshot.png](sq_screenshot.png)

## Pipeline status

[![pipeline status](https://gitlab.com/ntdesmond/s-23-lab-2/badges/master/pipeline.svg)](https://gitlab.com/ntdesmond/s-23-lab-2/-/commits/master)

## SonarCloud project

[![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-white.svg)](https://sonarcloud.io/summary/new_code?id=lab2_safonov)